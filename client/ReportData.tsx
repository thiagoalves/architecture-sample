import React from 'react';

class ReportData {
	customerName: string;
	hoursByProject: Map<string, number>;
	taskList: ReportTask[]
}

class ReportTask {
	date: Date;
	project: string;
	user: string;	
}
