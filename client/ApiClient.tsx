import React from 'react';

interface ApiClient {
	getReportData(customerId: number) : ReportData;
}

class FakeApiClient {
	getReportData(customerId: number) {
		// TODO: retornando dados fictícios para facilitar desenvolvimento e testes
		// no futuro vamos ler esses dados via API REST
		return {
			customerName: "Cliente 1",
			hoursByProject: {
				"Project 1": 418.333333,
				"Project 2": 500
			},
			taskList: [
				{
					name: "Task 1",
					project: "Project 1",
					user: "User 1",
					duration: 18.4
				},
				{
					name: "Task 2",
					project: "Project 2",
					user: "User 1",
					duration: 14.22
				}
			]
		};
	}
}