import React from 'react';

// Relatorio de horas por cliente e projeto
class CustomerHoursReport {
	_apiClient: Client;
	constructor(client: ApiClient) {
		// injecao de dependencia do cliente da API
		this._apiClient = client;
	}

	reportData: ReportData;
	onload() {
		const customerId = request['id'];
		this.reportData = this._apiClient.getReportData(customerId);
	}

	render() {
		return (
			<h1>{ reportData.customerName }</h1>
			<PieChart data={ reportData.hoursByProject } caption={ getChartCaption() } />
			<nav>
				<PageSize defaultValue="25" />
				<SearchField />
				<TableBody data={ reportData.taskList } headerTranslator=taskTranslator />
				<Pager />
			</nav>
		);
	}

	getChartCaption() {
		// TODO: usar uma biblioteca de i18n quando for necessário
		return 'Total de horas consumidas por projeto';
	}

	taskTranslator(taskField: string) {
		// TODO: usar uma biblioteca de i18n quando for necessário
		switch (taskField) {
			case 'date': return "Data";
			case 'project': return "Projeto";
			case 'user': return "Usuário";
			default: return taskField;
		}
	}
}
