import React from 'react';

loadApp() {
	// TODO: este é apenas um exemplo de como funcionaria a injeção de dependência

	// TODO: em vez de instanciar o FakeApiClient na mão,
	// usaríamos um framework de injeção de dependência aqui
	const apiClient = new FakeApiClient();
	const reportData = new ReportData(apiClient);
	app.load(reportData);
}