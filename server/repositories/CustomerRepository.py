from datetime import datetime
from models import Customer

class CustomerRepository(Customer customer):
    # traz a lista de tarefas aprovadas por cliente
    def get_approved_tasks(customer, page_size, page_index)
        # TODO: otimizar o ORM para trazer todos os dados de projetos e tarefas de uma vez só
        # atualmente executa uma query para cada projeto do cliente, e uma query para cada subtarefa
        # ou seja, 4 projetos com 5 tarefas cada causarao a execucao de 20 queries
        tasks = []
        for project in customer.projects:
            tasks.extend(project.approved_tasks)
        # TODO: fazer paginacao direto no banco de dados
        # atualmente traz todos os registros do banco para a memoria antes da paginacao
        return tasks[page_size:page_index]


	# calcula data de inicio do período atual
	def get_approved_hours_per_project(Customer customer):
		start_date = customer.get_start_date()
		end_date = customer.get_end_date()

        # TODO: trazer a soma da duração por projeto direto no banco
        tasks = Task.objects.filter(
            project__customer=self,
            approved=True,
            date__range=[start_date, end_date]
        )
        total_per_project = {}
        for t in tasks:
            # TODO: para cada tarefa, trazer o nome do cliente gera uma consulta nova no banco
            # (N+1 problem)
            project_name = t.project.name
            duration = t.duration_in_hours
            current_total = total_per_project.get(project_name)
            if (not current_total):
                current_total = 0
            current_total += t.duration_in_hours
            total_per_project.update(project_name, duration)

    return total_per_project