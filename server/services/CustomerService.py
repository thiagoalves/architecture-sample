class CustomerService():
	def __init__(customer_repository):
		this.customer_repository = customer_repository

	def approved_hours_per_project_report(customer_id, page_size, page_index):
		# Comportamento padrao da paginacao
		# Define como padrao tamanho de pagina como 10 e pagina inicial = 1
		if (not page_size):
			page_size = 10
		if (not page_index):
			page_index = 1

		customer = Customer(pk=customer_id)
		hours_per_project = customer_repository.get_approved_hours_per_project(customer)
		tasks = customer_repository.get_approved_tasks(customer, page_size, page_index)

		return {
			customerName: customer.name,
			hoursByProject: hours_per_project,
			taskList: tasks
		}