from datetime import datetime

class Customer(models.Model):
    name = models.CharField(max_length=50)

	# dia do mês em que o período de cálculo de horas se inicia
	cutoff_day = models.IntegerField(blank=False, null=False)

	# calcula data de inicio do período atual
	def get_start_date():
		return datetime.today().replace(day=cutoff_day)

	# calcula data de fim do período atual
	def get_end_date():
		# OBS: esta definicao poderia estar em outro lugar,
		# porem eh uma regra de negocio que pode a qualquer momento ser 
		# rediscutida com o cliente
		return datetime.today()
