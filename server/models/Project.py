class Project(models.Model):
    name = models.CharField(max_length=50)
    company = models.ForeignKey(Customer, related_name="customers")

    # lista de tarefas aprovadas para este projeto 
    def approved_tasks(self):
    	return Task.objects.filter(project=self, approved=True)
