class Task(models.Model):
    name = models.CharField(max_length=50)
    project = models.ForeignKey(Project, related_name="projects")
